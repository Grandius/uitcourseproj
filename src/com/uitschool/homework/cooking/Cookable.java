package com.uitschool.homework.cooking;

//интерфейс Cookable, содержащий метод void cook(String str)

public interface Cookable {

    void cook (String food);
}
