package com.uitschool.lesson15;

import java.util.Arrays;

public class FizbusTest {

    public static void main(String[] args) {

        /*int[] arrayTest = new int[100];
        int a = 1;
        for (int i = 0; i<arrayTest.length; i++) {
            arrayTest[i] = a;
            a++;
        }

        System.out.println("Initial integer array: ");
        System.out.println(Arrays.toString(arrayTest));*/

        String[] arrayTest = new String[100];
        int a = 1;
        for (int i = 0; i < arrayTest.length; i++) {
            arrayTest[i] = Integer.toString(a);
            a++;
        }
        System.out.println("Initial integer array: ");
        for (String testStr : arrayTest) {
            System.out.printf(testStr + " ");
        }

        System.out.println();

        for (String testStr : arrayTest) {
            if (Integer.parseInt(testStr) % 3 == 0 && Integer.parseInt(testStr) % 5 == 0) {

                testStr = "fizbuz";
                System.out.printf(testStr + " ");
            } else if (Integer.parseInt(testStr) % 3 == 0) {

                testStr = "fiz";
                System.out.printf(testStr + " ");
            } else if (Integer.parseInt(testStr) % 5 == 0) {

                testStr = "buz";
                System.out.printf(testStr + " ");
            } else {

                System.out.printf(testStr + " ");
            }

        }

        System.out.println();
        System.out.println("Alternative, without arrays");
        fizBuz();
    }

    public static void fizBuz() {

        int a = 1;
        while (a < 101) {

            if (a % 3 == 0 && a % 5 == 0) {
                System.out.printf("fizbuz" + " ");
            } else if (a % 3 == 0) {

                System.out.printf("fiz" + " ");
            } else if (a % 5 == 0) {

                System.out.printf("buz" + " ");
            } else {

                System.out.printf(a + " ");
            }
            a++;
        }
    }
}
