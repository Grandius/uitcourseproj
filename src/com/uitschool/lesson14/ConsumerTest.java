package com.uitschool.lesson14;

import com.uitschool.lesson7.Phone;

import java.util.function.Consumer;

public class ConsumerTest {

    public static void main(String[] args) {

        Consumer<Phone> consumer1 = phone -> System.out.println("Продали телефон: " + phone);
        Consumer<Phone> consumer2 = phone -> System.out.println("Купили на продажу телефон: " + phone);

        //consumer1.accept(new Phone("+380676488965", "Android phone", 760));
        consumer2.andThen(consumer1).accept(new Phone("+380676488965", "Android phone", 760));
        Consumer<String> consStr = System.out::println;
        consStr.accept("testString");




    }

}
