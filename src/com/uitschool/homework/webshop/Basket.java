package com.uitschool.homework.webshop;

/*
 * Создать класс Basket, содержащий массив купленных товаров.
 */

public class Basket {

    private Product[] purchProd;
    private int purchProdCount;

    public Basket(Product... purchProducts) {
        this.purchProd = purchProducts;
        purchProdCount = purchProd.length;
    }

    public Basket(int purchProdCount) {

        purchProd = new Product[purchProdCount];
        this.purchProdCount = purchProdCount;
    }

    public Product[] getPurchProd() {
        return purchProd;
    }

    public void setPurchProd(Product... purchProducts) {
        purchProd = purchProducts;
    }

    public int getPurchProdCount() {
        return purchProdCount;
    }

    public void setPurchProdCount(int purchProdCount) {
        this.purchProdCount = purchProdCount;
    }
}
