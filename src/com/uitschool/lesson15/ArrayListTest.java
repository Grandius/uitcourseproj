package com.uitschool.lesson15;

import com.uitschool.lesson7.Phone;

import java.util.ArrayList;
import java.util.List;

public class ArrayListTest {

    public static void main(String[] args) {

        List<Phone> phoneList = new ArrayList<>();
        phoneList.add(new Phone("+38974521","Samsung Galaxy S8", 45.58));
        phoneList.add(new Phone("+3894974521","Apple iPhone XI", 578.45));
        phoneList.add(new Phone("+698742521","Xiaomi Mi 10", 978.564));

        System.out.println(phoneList);
        System.out.println();
        System.out.println("phonelist numbers");

        for (Phone ph : phoneList) {

            System.out.println(ph.getNumber());
        }

        System.out.println();
        System.out.println("phonelist models");
        phoneList.forEach(p-> System.out.println(p.getModel()));
    }
}
