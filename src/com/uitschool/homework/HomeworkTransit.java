package com.uitschool.homework;

public class HomeworkTransit {

    //Создать программу, в которой последовательно прописать
    // преобразования всех совместимых примитивных типов друг к другу.
    // Преобразование для каждого типа вынесите в отдельный метод.

    public static void main(String[] args) {

        System.out.println(charToShort('{'));
        System.out.println(floatToChar(87.8453f));


    }

    static short byteToShort(byte b) {
        short sh = b;
        return sh;
    }

    static char byteToChar(byte b) {
        char ch = (char) b;
        return ch;
    }

    static int byteToInteger(byte b) {
        int i1 = b;
        return i1;
    }

    static long byteToLong(byte b) {
        long l1 = b;
        return l1;
    }

    static double byteToDouble(byte b) {
        double d1 = b;
        return d1;
    }

    static double byteToFloat(byte b) {
        float f1 = b;
        return f1;
    }

    static byte shortToByte(short sh) {
        byte b = (byte) sh;
        return b;
    }

    static char shortToChar(short sh) {
        char ch = (char) sh;
        return ch;
    }

    static int shortToInteger(short sh) {
        int i1 = sh;
        return i1;
    }

    static long shortToLong(short sh) {
        long l1 = sh;
        return l1;
    }

    static double shortToDouble(short sh) {
        double d1 = sh;
        return d1;
    }

    static float shortToFloat(short sh) {
        float f1 = sh;
        return f1;
    }

    static byte charToByte(char ch) {
        byte b1 = (byte) ch;
        return b1;
    }

    static short charToShort(char ch) {
        short sh = (short) ch;
        return sh;
    }

    static int charToInteger(char ch) {
        int i1 = ch;
        return i1;
    }

    static long charToLong(char ch) {
        long l1 = ch;
        return l1;
    }

    static double charToDouble(char ch) {
        double d1 = ch;
        return d1;
    }

    static float charToFloat(char ch) {
        float f1 = ch;
        return f1;
    }

    static byte integerToByte(int i) {
        byte b = (byte) i;
        return b;
    }

    static short integerToShort(int i) {
        short sh = (short) i;
        return sh;
    }

    static char integerToChar(int i) {
        char ch = (char) i;
        return ch;
    }

    static long integerToLong(int i) {
        long l1 = i;
        return l1;
    }

    static double integerToDouble(int i) {
        double d1 = i;
        return d1;
    }

    static float integerToFloat(int i) {
        float f1 = i;
        return f1;
    }

    static byte longToByte(long l) {
        byte b = (byte) l;
        return b;
    }

    static short longToShort(long l) {
        short sh = (short) l;
        return sh;
    }

    static char longToChar(long l) {
        char ch = (char) l;
        return ch;
    }


    static int longToInteger(long l) {
        int i1 = (int) l;
        return i1;
    }

    static double longToDouble(long l) {
        double d1 = l;
        return d1;
    }

    static float longToFloat(long l) {
        float f1 = l;
        return f1;
    }

    static byte floatToByte(float f) {
        byte b = (byte) f;
        return b;
    }

    static short floatToShort(float f) {
        short sh = (short) f;
        return sh;
    }

    //IDE не ругается на данное преобразование, но при проверке
    //чёткого символа преобразование не даёт
    static char floatToChar(float f) {
        char ch = (char) f;
        return ch;
    }

    static int floatToInteger(float f) {
        int i1 = (int) f;
        return i1;
    }

    static long floatToLong(float f) {
        long l1 = (long) f;
        return l1;
    }

    static double floatToDouble(float f) {
        double d1 = f;
        return d1;
    }

    static byte doubleToByte(double d) {
        byte b = (byte) d;
        return b;
    }

    static short doubleToShort(double d) {
        short sh = (short) d;
        return sh;
    }

    //IDE не ругается на данное преобразование, но при проверке
    //чёткого символа преобразование не даёт
    static char doubleToChar(double d) {
        char ch = (char) d;
        return ch;
    }

    static int doubleToInteger(double d) {
        int i1 = (int) d;
        return i1;
    }

    static long doubleToLong(double d) {
        long l1 = (long) d;
        return l1;
    }

    static float doubleToFloat(double d) {
        float f = (float) d;
        return f;
    }
}