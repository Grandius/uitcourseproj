package com.uitschool.lesson7;

public class PhoneDemo {

    public static void main(String[] args) {

        Phone phone1 = new Phone("784513323643", "Xiaomi", 567.9);
        Phone phone2 = new Phone("13456765498765", "Samsung");
        Phone phone3 = new Phone();

       /* Phone phone1 = new Phone();
        Phone phone2 = new Phone();*/

        phone1.setNumber("784513323643");
        /*phone1.model = "Xiaomi";
        phone1.weight = 567.9;

        phone2.number = "13456765498765";
        phone2.model = "Samsung";
        phone2.weight = 780.65;*/

        System.out.println(phone1.getNumber());
        System.out.println(phone1.getModel());
        System.out.println(phone1.getWeight());

        System.out.println(phone2.getNumber());
        System.out.println(phone2.getModel());
        System.out.println(phone2.getWeight());

        /*System.out.println(phone3.getNumber());
        System.out.println(phone3.model);
        System.out.println(phone3.weight);*/

        phone1.sendMessage("Hello Message", "4786321254531", "14784512963");
        System.out.println(Phone.getCount());

    }
}
