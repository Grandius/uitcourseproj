package com.uitschool.lesson3;

import java.util.Scanner;

public class IfDemo {
    public static void main(String[] args) {

        /*System.out.println("В аргументах программы: "+args[0]);
        int number = Integer.parseInt(args[0]);
        if(number%2!=0) {
            System.out.println(number);
        }
        System.out.println(args[1]);
        System.out.println(args[2]);*/
        Scanner input = new Scanner(System.in);
        int num;
        System.out.println("Please input natural number");
        if (input.hasNextInt()) {


            num = input.nextInt();
            if (num > 0 && num < 10) {
                System.out.println("Natural number " + num + " is less than 10");
            } else if (num > 0 && num > 10) {

                System.out.println("Natural number " + num + " is more than 10");

            } else {
                System.out.println("Number is negative");
            }

        } else {
            System.out.println("Please input correct natural number");
        }

    }
}
