package com.uitschool.homework.webshop;

public enum Operations {

    USER_AUTHENTICATE, ADD_PRODUCT_TO_BUSKET, FINISH_PURCHASE
}
