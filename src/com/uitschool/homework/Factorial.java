package com.uitschool.homework;

import java.util.Scanner;

/*
 * Реализовать подсчет факториала используя цикл for
 */
public class Factorial {

    public static void main(String[] args) {

        int testInt;
        Scanner input = new Scanner(System.in);
        System.out.println("Please input natural number");
        testInt = input.nextInt();
        System.out.println("Factorial of " + testInt + " is: " + factorial(testInt));
        input.nextLine();

        System.out.println("Please input natural number for factorialAlt");
        testInt = input.nextInt();
        System.out.println("FactorialAlt of " + testInt + " is: " + factorialAlt(testInt));

    }

    static long factorial(int n) {
        if (n == 0) {
            return 1;
        }
        if (n < 0) {
            System.out.println("Please input natural number");
        }
        return n * factorial(n - 1);
    }

    static long factorialAlt(int n) {

        /*if (n == 0) {
            return 1;
        }*/
        if (n < 0) {
            System.out.println("Please input natural number");
        }
        int ret = 1;
        for (int i = 1; i <= n; ++i) {

            ret *= i;
        }
        return ret;
    }
}
