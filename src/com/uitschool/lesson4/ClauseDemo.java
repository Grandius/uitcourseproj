package com.uitschool.lesson4;

import java.util.Objects;

public class ClauseDemo {
    public static void main(String[] args) {
        if ("1".equals(args[0])) {
            System.out.println("It's Monday!");
        } else if ("2".equals(args[0])) {
            System.out.println("It's Tuesday!");
        } else if ("3".equals(args[0])) {
            System.out.println("It's Wednesday!");
        } else if ("4".equals(args[0])) {
            System.out.println("It's Thursday!");
        } else if ("5".equals(args[0])) {
            System.out.println("It's Friday!");
        } else if ("6".equals(args[0]) || "7".equals(args[0])) {
            System.out.println("It's weekend!");
        } else {
            System.out.println("Error, please input number from 1 to 7");
        }

        System.out.println(Objects.equals(6, Integer.parseInt(args[0])));
    }
}
