package com.uitschool.homework.webshop;

/*
 * Создать несколько объектов класса Категория.
 * Создать объект класса User.
 */

import java.util.Scanner;

public class WebShopDemo {

    public static void main(String[] args) {

        Product product1 = new Product();
        Product product2 = new Product();
        Product product3 = new Product();
        Product product4 = new Product();
        Product product5 = new Product();
        Product product6 = new Product();
        Product product7 = new Product();
        Product product8 = new Product();
        Product product9 = new Product();

        ProdCategory category0 = new ProdCategory();
        ProdCategory category1 = new ProdCategory();
        ProdCategory category2 = new ProdCategory("Household");
        ProdCategory category3 = new ProdCategory("Dairy products", 15);
        ProdCategory category4 = new ProdCategory("Sweets", new Product[]{});
        category1.setCategName("Drinks");

        User user1 = new User();
        User user2 = new User("UserLogin", "UserPassword", new Basket());

        Scanner input = new Scanner(System.in);
        /*user1.authUser(input.nextLine(), input.nextLine());*/

        System.out.println("Trying to print categories");
        System.out.println("Category "+category1.getCategName()+": "+category1);
        System.out.println("Category "+category2.getCategName()+": "+category2);
        System.out.println("Category "+category3.getCategName()+": "+category3);
        System.out.println("Category "+category4.getCategName()+": "+category4);

        System.out.println("T");



    }
}
