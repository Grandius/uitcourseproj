package com.uitschool.lesson3;

import java.util.Scanner;

public class Median {

    public static void main(String[] args) {

        System.out.println("Program started");
        double sum = 0;
        float average = 0;
        //double[] numArray = null;
        /*String test = "4,56";
        Scanner sc = null;
        sc = new Scanner(test);
        sc.nextLine();
        Double.parseDouble()
        System.out.println("test of Scanner: "+sc.hasNextDouble()+" input "+sc.nextLine());
        sum = sc.nextDouble();
        System.out.println(sum);*/
        Scanner sc;

        if (args.length == 0 || args == null) {

            System.out.println("Please input three real numbers into args array");

        } else if (args.length == 3) {

            //numArray = new double[args.length];

            for (String test : args) {

                sc = new Scanner(test);

                if (sc.hasNextDouble()) {

                    sum += sc.nextDouble();


                } else {

                    System.out.println("Please input real number in XX,XX format, or the average will be incorrect");
                    break;
                }


            }


        } else {
            System.out.println("There must be exactly 3 real numbers in args array");
        }


        average = (float) sum / args.length;

        System.out.println("Average value of 3 real numbers: " + average);

        /*for (double d : numArray) {
            System.out.println(d);
        }*/


       /* if (args.length == 0 || args == null) {

            System.out.println("Please input three real numbers into args array");

        } else if (args.length == 3) {

            double[] numArray = new double[args.length];

            for (int i = 0; i < numArray.length; i++) {

                numArray[i] = Double.parseDouble(args[i]);
                sum += numArray[i];

            }

            average = (float) (sum / numArray.length);

            System.out.println("Среднее значение 3 чисел: " + average);


        }
        }*/

    }

}
