package com.uitschool.lesson15;

import com.uitschool.lesson7.Phone;

import java.util.SortedSet;
import java.util.TreeSet;

public class TreeSetTest {

    public static void main(String[] args) {
        SortedSet<Phone> phoneSet= new TreeSet<>();
        phoneSet.add(new Phone("+38974521","Samsung Galaxy S8", 45.58));
        phoneSet.add(new Phone("+3894974521","Apple iPhone XI", 578.45));
        phoneSet.add(new Phone("+698742521","Xiaomi Mi 10", 978.564));

        //System.out.println(phoneSet);
        phoneSet.forEach(System.out::println);
    }
}
