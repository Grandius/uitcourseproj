package com.uitschool.homework;

public class Homework {

    /*
    * Выполнение домашнего задания для курса Java 20-01
    * от Ukrainian IT School
    * студентом Кожановым Адрианом
    * */

    public static void main(String[] args) {

        double d1 = 4.21675; //если тут написать 4.21675f, то d1 спокойно то значение примет
        /*float f1 = 55.876f;
        char c2 = 45;
        char с3 = 'a';
        boolean b = false;
        String s1 = "Hello!!";*/
        System.out.println("Логический литерал: " + true);
        System.out.println("Строковый  литерал: " + "Hello World");
        System.out.println("Символьный литерал: " + 'a');
        System.out.println("Целочисленный (2) литерал: " + 0b1001);
        System.out.println("Целочисленный (8) литерал: " + 0101);
        System.out.println("Целочисленный (10) литерал: " + 98);
        System.out.println("Целочисленный (16) литерал: " + 0XBa98);
        System.out.println("Целочисленный (16) литерал: " + 0XAb12);
        System.out.println("double литерал: " + 4.21675);
        System.out.println("float литерал: " + 55.876f);
        int x = getSum(100, 58);
        System.out.println("сложение: " + x);

        }

    static int getSum(int a, int b) {

        return a + b;
    }

    static int getDiff(int a, int b) {

        return a - b;
    }

    static int getMult(int a, int b) {

        return a * b;
    }

    static float getDiv(int a, int b) {

        return a/b;
    }
}
