package com.uitschool.homework;

//Создать несколько объектов класса Circle

public class CircleTest {

    public static void main(String[] args) {

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(45.6);
        Circle circle3 = new Circle(8.741);
        Circle circle4 = new Circle(2.69);
        Circle circle5 = new Circle(25.412554);
        System.out.println("Info about circle1: " + "radius is "+circle1.getRadius() + ", circumference is "+circle1.getCircumference()+ ", area is "+circle1.getArea());
        System.out.println("Info about circle2: " + "radius is "+circle2.getRadius() + ", circumference is "+circle2.getCircumference()+ ", area is "+circle2.getArea());
        System.out.println("Info about circle3: " + "radius is "+circle3.getRadius() + ", circumference is "+circle3.getCircumference()+ ", area is "+circle3.getArea());
        System.out.println("Info about circle4: " + "radius is "+circle4.getRadius() + ", circumference is "+circle4.getCircumference()+ ", area is "+circle4.getArea());
        System.out.println("Info about circle5: " + "radius is "+circle5.getRadius() + ", circumference is "+circle5.getCircumference()+ ", area is "+circle5.getArea());
    }
}
