package com.uitschool.homework.seasons;

public class SeasonTest {

    public static void main(String[] args) {

        Season favSeason = Season.SUMMER;
        System.out.println(favSeason);

        Season season1 = Season.valueOf("WINTER");
        System.out.println(season1);

        printTest(Season.WINTER);
        getDescriptionAlt(favSeason);


        System.out.println("Alternative method of outputting information about seasons");
        for (Season testSeason1 : Season.values()) {
            getDescriptionAlt(testSeason1);
            System.out.println("Average temperature in " + testSeason1.toString().toLowerCase() + " is: " + testSeason1.getAvgTemperature());
        }

        System.out.println("Standard method of outputting information about seasons");
        for (Season testSeason2 : Season.values()) {
            System.out.println("Season " + testSeason2.toString().toLowerCase() + " is " + testSeason2.getDescription().toLowerCase()
                    + " with average temperature of " + testSeason2.getAvgTemperature() + " Celsius");

        }

    }

    public static void printTest(Season season) {

        switch (season) {
            case WINTER:
                System.out.println("I like " + Season.WINTER.toString().toLowerCase()
                        + "!");
                break;
            case SPRING:
                System.out.println("I like " + Season.SPRING.toString().toLowerCase()
                        + "!");
                break;
            case SUMMER:
                System.out.println("I like " + Season.SUMMER.toString().toLowerCase()
                        + "!");
                break;
            case AUTUMN:
                System.out.println("I like " + Season.AUTUMN.toString().toLowerCase()
                        + "!");
                break;
        }
    }

    public static void getDescriptionAlt(Season season) {

        switch (season) {
            case WINTER:
            case AUTUMN:
                System.out.println("Season " + season.toString().toLowerCase() + " is a cold time of the year.");
                break;

            case SPRING:
            case SUMMER:
                System.out.println("Season " + season.toString().toLowerCase() + " is a warm time of the year.");
                break;

        }
    }
}
