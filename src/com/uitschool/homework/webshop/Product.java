package com.uitschool.homework.webshop;

/*
 * Создать класс Товар, имеющий переменные имя, цена, рейтинг.
 */
public class Product {

    private String prodName;
    private double prodPrice;
    private int prodRating;
    private String prodCategory;

    public Product(String prodName, double prodPrice, int prodRating, String prodCategory) {

        this.prodName = prodName;
        this.prodPrice = prodPrice;
        this.prodRating = prodRating;
        this.prodCategory = prodCategory;
    }

    public Product() {

    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public double getProdPrice() {
        return prodPrice;
    }

    public void setProdPrice(double prodPrice) {
        this.prodPrice = prodPrice;
    }

    public int getProdRating() {
        return prodRating;
    }

    public void setProdRating(int prodRating) {
        this.prodRating = prodRating;
    }

    public String getProdCategory() {
        return prodCategory;
    }

    public void setProdCategory(String prodCategory) {
        this.prodCategory = prodCategory;
    }

    @Override
    public String toString() {
        return "Product{" +
                "prodName='" + prodName + '\'' +
                ", prodPrice=" + prodPrice +
                ", prodRating=" + prodRating +
                ", prodCategory='" + prodCategory + '\'' +
                '}';
    }
}
