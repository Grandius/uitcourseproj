package com.uitschool.lesson12;

import java.util.Formatter;

public class FormatterTest {
    public static void main(String[] args) {

        printF("Antonov", 4, "Chemistry");
        printF("Petrikovskaya", 5, "Mathematics");

    }

    private static void printF(String fullName, int mark, String subject) {

        Formatter form = new Formatter();
        System.out.printf("Student %-15s received %3s on %10s.\n", fullName, mark, subject);
        System.out.format("Student %-15s received %3s on %10s.\n", fullName, mark, subject);
    }
}
