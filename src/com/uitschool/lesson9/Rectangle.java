package com.uitschool.lesson9;

import java.util.Objects;

public class Rectangle extends Shape {

    private int x1, y1, x2, y2, x3, y3, x4, y4;

    public Rectangle(String color, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4) {

        super(color);
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
        this.x4 = x4;
        this.y4 = y4;
    }

    public Rectangle() {

        super();
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getX3() {
        return x3;
    }

    public void setX3(int x3) {
        this.x3 = x3;
    }

    public int getY3() {
        return y3;
    }

    public void setY3(int y3) {
        this.y3 = y3;
    }

    public int getX4() {
        return x4;
    }

    public void setX4(int x4) {
        this.x4 = x4;
    }

    public int getY4() {
        return y4;
    }

    public void setY4(int y4) {
        this.y4 = y4;
    }

    @Override
    public void draw() {

        System.out.println("Draw rectangle with color " + getColor() +
                " in coordinates: A ("
                + getX1() + ", " + getY1() + ")" + ", B (" +
                getX2() + ", " + getY2() + ")" + ", C (" +
                getX3() + ", " + getY3() + ")" + ", D (" +
                getX4() + ", " + getY4() + ")");


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Rectangle rectangle = (Rectangle) o;
        return getX1() == rectangle.getX1() &&
                getY1() == rectangle.getY1() &&
                getX2() == rectangle.getX2() &&
                getY2() == rectangle.getY2() &&
                getX3() == rectangle.getX3() &&
                getY3() == rectangle.getY3() &&
                getX4() == rectangle.getX4() &&
                getY4() == rectangle.getY4();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getX1(), getY1(), getX2(), getY2(), getX3(), getY3(), getX4(), getY4());
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "x1=" + x1 +
                ", y1=" + y1 +
                ", x2=" + x2 +
                ", y2=" + y2 +
                ", x3=" + x3 +
                ", y3=" + y3 +
                ", x4=" + x4 +
                ", y4=" + y4 +
                "} " + super.toString();
    }
}
