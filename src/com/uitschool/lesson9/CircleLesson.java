package com.uitschool.lesson9;

public class CircleLesson extends Shape {

    private int x, y;
    private double radius;

    public CircleLesson(String color, int x, int y, double radius) {

        super(color);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public CircleLesson() {
        super();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void draw() {

        System.out.println("Draw circle with color " + getColor() +
                ", radius: " + getRadius() + " in coordinates: ("
                + getX() + ", " + getY() + ")");

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CircleLesson circleLesson = (CircleLesson) o;

        if (getX() != circleLesson.getX()) return false;
        if (getY() != circleLesson.getY()) return false;
        return Double.compare(circleLesson.getRadius(), getRadius()) == 0;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        result = 31 * result + getX();
        result = 31 * result + getY();
        temp = Double.doubleToLongBits(getRadius());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "CircleLesson{" +
                "x=" + x +
                ", y=" + y +
                ", radius=" + radius +
                "} " + super.toString();
    }
}
