package com.uitschool.homework.webshop;

/*
 * Создать класс User, содержащий логин, пароль и объект класса Basket.
 */

public class User {

    private String userLogin;
    private String userPassword;
    private Basket userBasket;

    public User(String userLogin, String userPassword, Basket userBasket) {

        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userBasket = userBasket;
    }


    public User() {

    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Basket getUserBasket() {
        return userBasket;
    }

    public void setUserBasket(Basket userBasket) {
        this.userBasket = userBasket;
    }

    public void authUser(String userInputLogin, String userInputPassword) {

        System.out.println("Input your login");
        userLogin = userInputLogin;
        System.out.println("Input your password");
        userPassword = userInputPassword;
    }
}
