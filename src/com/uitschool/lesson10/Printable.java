package com.uitschool.lesson10;

public interface Printable {

    void print();
}
