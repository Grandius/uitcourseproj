package com.uitschool.homework;

import java.util.Arrays;
//unfinished

public class Matrix {

    double[][] initArray;
    int rows;
    int columns;

    public Matrix(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        initArray = new double[rows][columns];
    }

    public Matrix() {

    }

    public static void main(String[] args) {

        Matrix m1 = new Matrix(2, 4);
        Matrix m2 = new Matrix(2, 4);

        /*for (double[] tempMat: m1.initArray) {
            for (double tempNum: tempMat) {
                tempNum = Math.random()*100;
            }
        }*/

        matrInit(m1);
        matrInit(m2);
        System.out.println("First matrix: ");
        m1.print();
        System.out.println("Second matrix: ");
        m2.print();
        Matrix m3 = new Matrix();
        m3.initArray = matrMultByMatr(m1, m2);
        m3.print();





        /*for (int i = 0; i < m1.initArray.length; i++) {
            for (int j = 0; j < m1.initArray[0].length; j++) {
                System.out.format("%6d ", m1.initArray[i][j]);
            }
            System.out.println();
        }*/

        /*for (double[] tempMat: m1.initArray) {
            System.out.println(Arrays.toString(tempMat));
        }*/

    }

    static double[][] matrMultByMatr(Matrix a, Matrix b) {

        double[][] multMatrRes = null;

        if (a.equals(null) || b.equals(null) || a == null || b == null) {
            System.out.println("For the multiplication operation please initialize both matrixes");
        } else {

            int m = a.initArray.length;
            int n = b.initArray[0].length;
            int o = b.initArray.length;
            multMatrRes = new double[m][n];

            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    for (int k = 0; k < o; k++) {
                        multMatrRes[i][j] += a.initArray[i][k] * b.initArray[k][j];
                    }
                }
            }


        }
        if (multMatrRes.equals(null) || multMatrRes == null) {
            System.out.println("There was an error in execution, program needs to run again");
            matrMultByMatr(a, b);
        }
        return multMatrRes;
    }

    void matrMultByMatrExtra(Matrix a, Matrix b) {

        double[][] multMatrRes = null;

        if (a.equals(null) || b.equals(null) || a == null || b == null) {
            System.out.println("For the multiplication operation please initialize both matrixes");
        } else {

            int m = a.initArray.length;
            int n = b.initArray[0].length;
            int o = b.initArray.length;
            multMatrRes = new double[m][n];

            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    for (int k = 0; k < o; k++) {
                        multMatrRes[i][j] += a.initArray[i][k] * b.initArray[k][j];
                    }
                }
            }


        }
        if (multMatrRes.equals(null) || multMatrRes == null) {
            System.out.println("There was an error in execution, program needs to run again");
            matrMultByMatr(a, b);
        }
        this.initArray=multMatrRes;



       /* for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[0].length; j++) {
                System.out.format("%6d ", res[i][j]);
            }
            System.out.println();
        }*/


    }

    double[][] matrMultByNum(Matrix matr, double num) {



       /* for (double[] tempArr : matr.initArray) {
            for (double tempNum : tempArr) {
                tempNum *= num;
            }
        }*/

        double[][] multByNumRes = matr.initArray;

        for (int i = 0; i < matr.initArray.length; i++) {
            for (int j = 0; j < matr.initArray[0].length; j++) {
                multByNumRes[i][j] = matr.initArray[i][j] * num;
            }
        }
        return multByNumRes;


    }


    void print() {

        for (int i = 0; i < initArray.length; i++) {
            for (int j = 0; j < initArray[0].length; j++) {
                System.out.format("%f", initArray[i][j]);
            }
            System.out.println();
        }

    }

    static void matrInit(Matrix matr) {

        for (int i = 0; i < matr.initArray.length; i++) {
            for (int j = 0; j < matr.initArray[0].length; j++) {
                matr.initArray[i][j] = Math.random() * 100;
            }
        }
    }
}
