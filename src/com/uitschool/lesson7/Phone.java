package com.uitschool.lesson7;

import java.util.Arrays;
import java.util.Objects;

public class Phone  implements Comparable<Phone>{

    private int id;
    private String number;
    private String model;
    private double weight;
    private static int count;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Phone.count = count;
    }

    public Phone(String num, String mod, double w) {

        this(num,mod);
        /*number = num;
        model = mod;*/
        weight = w;
    }

    public Phone(String num, String mod) {

        number = num;
        model = mod;
        count++;
    }

    Phone() {
        count++;
    }

    void receiveCall(String name) {
        System.out.println("Calling " + name);
    }

    void receiveCall(String name, String number) {
        System.out.println("Calling " + name + " with number" + number);
    }


    void sendMessage(String message, String... numbers) {

        System.out.println("Message "+message+" is sent to numbers: "+Arrays.toString(numbers));
    }

    @Override
    public String toString() {
        return "Phone{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Double.compare(phone.getWeight(), getWeight()) == 0 &&
                Objects.equals(getNumber(), phone.getNumber()) &&
                Objects.equals(getModel(), phone.getModel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumber(), getModel(), getWeight());
    }

    @Override
    public int compareTo(Phone o) {
        return Double.compare(this.getWeight(),o.getWeight());
    }
}
