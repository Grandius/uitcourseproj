package com.uitschool.lesson13;

public class GenerTest<T> {

    private T[] array;

    public GenerTest(T[] array) {
        this.array = array;
    }

    public double average() {
        double sum = 0.0;

        for (T value : array) {

            if(value instanceof Number) {
                sum += ((Number)value).doubleValue();
            }

        }

        return sum / array.length;
    }

    public static void main(String[] args) {
        Integer[] intArray = {1, 2, 3, 4, 5};
        GenerTest<Integer> integerAverage = new GenerTest<>(intArray);
        System.out.println("Среднее значения для Integer " + integerAverage.average());

        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5};
        GenerTest<Double> doubleAverage = new GenerTest<>(doubleArray);
        System.out.println("Среднее значения для Double " + doubleAverage.average());

        // Не откомпилируется,
        // потому что String не является наследником Number
       String[] strArray = {"1", "2", "3", "4", "5"};
        GenerTest<String> strAverage = new GenerTest<>(strArray);

        System.out.println("Среднее значения для String " + strAverage.average());
    }


}
