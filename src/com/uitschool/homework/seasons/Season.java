package com.uitschool.homework.seasons;

public enum Season {
    AUTUMN(9.6), WINTER(-10.2), SPRING(15.0), SUMMER(29.9) {
        @Override
        public String getDescription() {
            return "Warm time of the year";
        }
    };

    private float avgTemperature;

    Season(double avgTemperature) {
        this.avgTemperature = (float) avgTemperature;
    }

    Season() {
        this.avgTemperature = 0;
    }

    public float getAvgTemperature() {
        return avgTemperature;
    }

    public String getDescription() {
        return "Cold time of the year";
    }
}
