package com.uitschool.lesson14;

public class InnerClassTest {

    private static String login, password;

    public static class Query {
        public void printLog() {

            System.out.printf("User with login %s and password %s sent query",
                    login, password);
        }
    }

    public static void initialize(String login, String password) {
        InnerClassTest.login = login;
        InnerClassTest.password = password;
    }

    public InnerClassTest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password.charAt(0) + "*****";
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*public void createQuery() {
        class Query {
            public void printLog() {

                System.out.printf("User with login %s and password %s sent query", getLogin(), getPassword());
            }
        }
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InnerClassTest that = (InnerClassTest) o;

        if (getLogin() != null ? !getLogin().equals(that.getLogin()) : that.getLogin() != null) return false;
        return getPassword() != null ? getPassword().equals(that.getPassword()) : that.getPassword() == null;
    }

    @Override
    public int hashCode() {
        int result = getLogin() != null ? getLogin().hashCode() : 0;
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InnerClassTest{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public static void main(String[] args) {

        //InnerClassTest test1 = new InnerClassTest("login1","passwrd1");
        initialize("login1", "password1");
        InnerClassTest.Query q1 = new InnerClassTest.Query();
        q1.printLog();
    }
}
