package com.uitschool.lesson12;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//пздц
public class PatternTest {
    public static void main(String[] args) {
        String str = "Versions: Java 5, Java 6, Java 7, Java 8, Java 12";
        String patternStr = "Java\\s+\\d{1,2}";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }
}
