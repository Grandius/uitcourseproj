package com.uitschool.homework.cooking;

public class FoodTest {

    public static void main(String[] args) {
        Food sandwich = new Food("sandwich");
        Food food1 = new Food();
        Cookable test = new Cookable() {
            @Override
            public void cook(String food) {
                System.out.println("The dish " + food + " is cooked");
            }
        };

        sandwich.prepare(test,"meat");
        food1.prepare(test,"something");
    }
}
