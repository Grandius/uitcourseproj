package com.uitschool.lesson8;

public class StudentDemo {

    public static void main(String[] args) {

        Student std1 = new Student("Sergey", "Nikonov", "Computer Science", 4.1);
        Aspirant asp1 = new Aspirant("Alla", "Fedorova", "Computer Security", 4.67, "ScienceWork1");
        Student asp2 = new Aspirant("Fedor", "Fedorovich", "Computer Security", 4.02, "ScienceWork2");

        Student[] students = {std1, asp1, asp2};


        System.out.println(asp1.firstName + " " + asp1.lastName + " " + asp1.group + " " +
                asp1.averageMark + " " + asp1.scienceWork);
        System.out.println(asp2.firstName + " " + asp2.lastName + " " + asp2.group + " " +
                asp2.averageMark/*+ " " asp2.scienceWork*/);

        for (Student tempStudent : students) {
            System.out.println(tempStudent.firstName + " " +
                    asp1.lastName + " gets scholarship: " +
                    tempStudent.getScholarship());
        }
    }
}
