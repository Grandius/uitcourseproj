package com.uitschool.lesson14;

import java.util.function.Predicate;

public class PredicateTest {

    public static void main(String[] args) {

        Predicate<String> pred1 = s -> s != null;
        Predicate<String> pred2 = s -> !s.isEmpty();
        Predicate<String> pred3 = s -> !(s == null || s.isEmpty());

        Predicate<String> pred1And2 = pred1.and(pred2);

        /*System.out.println("Test1 pred1 is not null: " + pred1.test(null));
        System.out.println("Test2 pred1 is not null: " + pred1.test(""));
        System.out.println("Test3 pred1 is not null: " + pred1.test("testString"));

        //System.out.println("Test1 pred2 is not null: "+ pred2.test(null));
        System.out.println("Test2 pred2 is not empty: " + pred2.test(""));
        System.out.println("Test3 pred2 is not empty: " + pred2.test("testString"));

        System.out.println("Test2 pred3 is not null or empty: " + pred3.test(""));
        System.out.println("Test1 pred3 is not null or empty: " + pred3.test(null));
        System.out.println("Test3 pred3 is not null or empty: " + pred3.test("testString"));

        System.out.println("Test1 predCont is not null or empty: " + pred1And2.test(null));
        System.out.println("Test2 predCont is not null or empty: " + pred1And2.test(""));
        System.out.println("Test3 predCont is not null or empty: " + pred1And2.test("testString"));*/

        Predicate<String> testPredStartJ = s -> s.startsWith("J");
        Predicate<String> testPredStartN = s -> s.startsWith("N");
        Predicate<String> testPredEndA = s -> s.endsWith("A");

        Predicate<String> combTestPred = testPredStartJ.or(testPredStartN).and(testPredEndA);
        System.out.println("Test string " + "someString" + " with combined predicate: " + combTestPred.test("someString"));
        System.out.println("Test string " + "JustA" + " with combined predicate: " + combTestPred.test("JustA"));
        System.out.println("Test string " + "NiceHA" + " with combined predicate: " + combTestPred.test("NiceHA"));

    }
}
