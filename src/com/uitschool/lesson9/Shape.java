package com.uitschool.lesson9;

public abstract class Shape {

    private String color;

    public Shape(String color) {
        this.color = color;
    }

    public Shape() {

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    protected abstract void draw();

    @Override
    public String toString() {
        return "Shape{" +
                "color='" + color + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shape shape = (Shape) o;

        return getColor() != null ? getColor().equals(shape.getColor()) : shape.getColor() == null;
    }

    @Override
    public int hashCode() {
        return getColor() != null ? getColor().hashCode() : 0;
    }

    public static void main(String[] args) {
        Shape[] forms = new Shape[3];
        CircleLesson circle1 = new CircleLesson("red", 4, 5, 10);
        CircleLesson circle2 = new CircleLesson("green", 5, 8, 16);
        CircleLesson circle3 = new CircleLesson("red", 4, 5, 10);
        Rectangle rectangle1 = new Rectangle("blue", 5, 3, 4, 5, 10, 20, 13, 4);

        forms[0] = circle1;
        forms[1] = circle2;
        forms[2] = rectangle1;

        for (Shape form : forms) {
            //form.draw();
            System.out.println(form);
        }

        System.out.println(circle1.equals(circle3));
        System.out.println(circle1 == circle3);

    }
}
