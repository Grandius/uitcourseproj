package com.uitschool.lesson14;

import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

public class FunctionTest {

    public static void main(String[] args) {

        Function<Integer, String> checkFunction = i -> {
            if (i > 0) {
                return "Number is natural";
            } else if (i < 0) {
                return "Number is negative";

            } else return "Number is zero";
        };

        int[] testArray = {-2, -1, 0, 1, 2};

        for (int testNum : testArray) {

            System.out.println("Number " + testNum + " check: " + checkFunction.apply(testNum));
        }


        /*Supplier<Integer> supplierInt= () -> (int) (Math.random()*10);

        for (int i =0; i<5; i++){
            System.out.println(supplierInt.get());
        }*/
        IntSupplier supplierInt = () -> (int) (Math.random() * 10);

        for (int i = 0; i < 5; i++) {
            System.out.println(supplierInt.getAsInt());
        }

        IntFunction<String> function = String::valueOf;
        System.out.println(function.apply(450));

        Supplier<String> newString = String::new;
        System.out.println(newString.get());



    }
}
