package com.uitschool.homework;

//Создать класс Circle

public class Circle {

    private double radius; //переменная radius
    private final double pi = Math.PI; //константа PI

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {

    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getPi() {
        return pi;
    }

    public double getDiameter() {
        return getRadius() * 2;
    }

    //методы, вычисляющие площадь и длину окружности;

    public double getCircumference() {

        return getDiameter() * getPi();
    }

    public double getArea() {

        return Math.pow(getRadius(),2) * getPi();
    }
}