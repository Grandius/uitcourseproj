package com.uitschool.lesson10;

import java.util.Objects;

public class Magazine implements Printable {

    private String magazineName;

    public Magazine(String magazineName) {
        this.magazineName = magazineName;
    }

    public Magazine() {

    }

    public String getMagazineName() {
        return magazineName;
    }

    public void setMagazineName(String magazineName) {
        this.magazineName = magazineName;
    }

    public static void printMagazines(Printable[] printables) {
        for (Printable print:printables) {
            if (print instanceof Magazine)
            System.out.println(((Magazine) print).getMagazineName());
        }
    }
    @Override
    public void print() {

        System.out.println("It's a magazine: " + getMagazineName());

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Magazine magazine = (Magazine) o;
        return Objects.equals(getMagazineName(), magazine.getMagazineName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMagazineName());
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "magazineName='" + magazineName + '\'' +
                '}';
    }
}
