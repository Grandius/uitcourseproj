package com.uitschool.lesson4;

public class WhileTask {

    public static void main(String[] args) {
        /*int n = 1;
        while (n <11) {
            System.out.println("Task" + n++);
        }*//*
        int n = 1;
        do {
            System.out.println(n++);
        } while (n <= 100);*/

        for (int i = 1, j = 5; i <= j; i++, j--) {
            System.out.println("i = " + i);
            System.out.println("j = " + j);
        }
        //char sym = 'g';
        //System.out.println((int)'b');
        for (int i = 97; i < 123; i++) {
            System.out.print((char)i + " ");
        }
        Math.random();
    }
}
