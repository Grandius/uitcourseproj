package com.uitschool.lesson13;

import java.time.LocalDate;

public class NewLecture {
    public static void main(String[] args) {
        LocalDate dob = LocalDate.of(1994, 11,14);
        System.out.println(dob);

        System.out.println(dob.getDayOfWeek());
        System.out.println(dob.getDayOfMonth());
        System.out.println(dob.getDayOfYear());
        System.out.println(dob.getMonth());
        System.out.println(dob.getYear());

        LocalDate today = LocalDate.now();

        System.out.println(today.isAfter(dob));
        System.out.println(today.isBefore(dob));
        System.out.println(today.isEqual(dob));

    }
}
