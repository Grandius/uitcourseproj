package com.uitschool.lesson13;

public class ThrGener<T extends String, V extends Animal, L extends Number> {

    private T obj1;
    private V obj2;
    private L obj3;

    public ThrGener(T obj1, V obj2, L obj3) {

        this.obj1 = obj1;
        this.obj2 = obj2;
        this.obj3 = obj3;
    }

    public T getObj1() {
        return obj1;
    }

    public V getObj2() {
        return obj2;
    }

    public L getObj3() {
        return obj3;
    }

    public void showName() {

        System.out.println("obj1: " + getObj1().getClass().getName());
        System.out.println("obj2: " + getObj2().getClass().getName());
        System.out.println("obj3: " + getObj3().getClass().getName());
    }

    public static void main(String[] args) {

        //ThrGener<Integer, Float, String> generTest = new ThrGener<>(56, 23.457f, "test");
        //System.out.println(generTest.getClass().getName());

        ThrGener<String, Cat, Integer> generTest = new ThrGener<>("test", new Cat(), 56);
        generTest.showName();

    }
}
