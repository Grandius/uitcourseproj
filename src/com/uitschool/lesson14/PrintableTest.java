package com.uitschool.lesson14;

import com.uitschool.lesson10.Book;
import com.uitschool.lesson10.Magazine;
import com.uitschool.lesson10.Printable;

public class PrintableTest {

    public static void main(String[] args) {

        Book b1 = new Book("Some stories");
        Book b2 = new Book("Some stories 2: Electric Boogaloo");
        Magazine m1 = new Magazine("Weekly News");
        Magazine m2 = new Magazine("Daily News");
        Printable n1 = () -> System.out.println("It's a newspaper");
        Printable[] printables = {b1, b2, m1, m2, n1};

        for (Printable test : printables) {
            test.print();
        }
    }
}
