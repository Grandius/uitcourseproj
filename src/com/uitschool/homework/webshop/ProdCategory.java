package com.uitschool.homework.webshop;

/*
 * Создать класс Категория, имеющий переменные имя и массив товаров.
 */

import java.util.Arrays;

public class ProdCategory {

    private String categName;
    //private ArrayList<Product> prodArray;
    private Product[] products;
    private int categProdCount;

    public ProdCategory(String categName, int categProdCount) {

        this.categName = categName;
        this.categProdCount = categProdCount;
        products = new Product[categProdCount];
    }

    public ProdCategory(String categName, Product... products) {

        this.categName = categName;
        this.products = checkProducts(categName, products);
        categProdCount = this.products.length;
    }

    public ProdCategory() {

    }

    private Product[] checkProducts(String necCategName, Product... products) {

        Product[] retProd = new Product[products.length];
        for (int i = 0; i < products.length; i++) {

            if (products[i].getProdCategory().equals(necCategName)) {

                retProd[i] = products[i];
            } else {
                System.out.println("The products in one category should have the same category name");
                break;
            }
        }

        return retProd;
    }

    public String getCategName() {
        return categName;
    }

    public void setCategName(String categName) {
        this.categName = categName;
    }

    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product... products) {
        this.products = products;
        this.categProdCount = products.length;
    }

    public int getCategProdCount() {
        return categProdCount;
    }

    public void setCategProdCount(int categProdCount) {
        this.categProdCount = categProdCount;
    }


    @Override
    public String toString() {
        return "ProdCategory{" +
                "categName='" + categName + '\'' +
                ", products=" + Arrays.toString(products) +
                ", categProdCount=" + categProdCount +
                '}';
    }
}
