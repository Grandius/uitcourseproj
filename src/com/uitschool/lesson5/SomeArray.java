package com.uitschool.lesson5;

import java.util.Arrays;

public class SomeArray {
    public static void main(String[] args) {
        String daysArray[] = new String[7];
        daysArray[0] = "Sunday";
        daysArray[1] = "Monday";
        daysArray[2] = "Tuesday";
        daysArray[3] = "Wednesday";
        daysArray[4] = "Thursday";
        daysArray[5] = "Friday";
        daysArray[6] = "Saturday";

        /*double dblArray[] = {5.34567, 2345646.648967956, 89.00, 123};
        for (double d : dblArray) {
            System.out.println(" " + d);
        }*/

        /*char symbol = 'A';

        String strArray2D[][] = new String[10][7];
        for (int i = 0; i < strArray2D.length; i++) {
            for (int j = 0; j < strArray2D[0].length; j++) {

                strArray2D[i][j] = symbol + "" + (j + 1);

                System.out.print(strArray2D[i][j] + " ");

            }
            symbol++;
            System.out.println();
        }*/
        /*int[][] array = new int[4][];
        array[0] = new int[1];
        array[1] = new int[2];
        array[2] = new int[3];
        array[3] = new int[4];
        int i, j, k = 0;
        for (i = 0; i < 4; i++) {
            for (j = 0; j < i + 1; j++) {
                array[i][j] = k++;
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

        for (int[] arrayD : array) {

            for (int elem : arrayD) {

                System.out.print(elem + " ");
            }

            System.out.println();
        }*/
        char[][] chrArray = {{'a', 'b'},
                {'c', 'd'},
                {'e', 'f'},
                {'g', 'h'}};
        System.out.println(Arrays.deepToString(chrArray));

        for (char[] row : chrArray) {

            for (char elem : row) {

                System.out.print(elem + " ");
            }

            System.out.println();
        }
    }
}
