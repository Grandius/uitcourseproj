package com.uitschool.homework;

import java.util.Scanner;

/*
 * Создать программу, которая будет выводить на экран
 * меньшее по модулю из трёх введённых пользователем вещественных чисел.
 * Для вычисления модуля используйте  тернарную условную операцию.
 */

public class HomeworkOne {

    public static void main(String[] args) {

        double firstNum = 0;
        double secondNum = 0;
        double thirdNum = 0;
        Scanner input = new Scanner(System.in);

        System.out.println("Enter first real number");
        firstNum = returnDouble(input);
        System.out.println("Enter second real number");
        secondNum = returnDouble(input);
        System.out.println("Enter third real number");
        thirdNum = returnDouble(input);
        System.out.println("Absolute smallest of three real numbers: " + returnSmallestAbs(firstNum, secondNum, thirdNum));

    }

    static double returnSmallestAbs(double a, double b, double c) {

        double smallAbs = 0;
        double absFirst = a < 0 ? -a : a;
        double absSecond = b < 0 ? -b : b;
        double absThird = c < 0 ? -c : c;

        if (absFirst < absSecond && absFirst < absThird) {
            smallAbs = absFirst;
        } else if (absSecond < absFirst && absSecond < absThird) {
            smallAbs = absSecond;
        } else if (absThird < absFirst && absThird < absSecond) {
            smallAbs = absThird;
        }

        return smallAbs;

        /*boolean[] smallAbsCheck = new boolean[3];
        smallAbsCheck[0] = a<b&&a<c;
        smallAbsCheck[1] = b<a&&b<c;
        smallAbsCheck[2] = c<a&&c<b;
        if (true == a < b && a < c) {
            smallAbs = (float) a;
        }*/

    }

    static double returnDouble(Scanner sc) {

        double retNum = 0;
        if (sc.hasNextDouble()) {
            retNum = sc.nextDouble();
            sc.nextLine();
        } else if (!sc.hasNextDouble()) {
            System.out.println("Error has occured, please input correct real number in XX,XX form");
            sc.nextLine();
            retNum = returnDouble(sc);
        }

        if (retNum == 0) {

            System.out.println("Error has occured, please input correct real number in XX,XX form");
            retNum = returnDouble(sc);
        }

        return retNum;
    }
}