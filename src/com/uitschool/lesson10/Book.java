package com.uitschool.lesson10;

import java.util.Objects;

public class Book implements Printable {

    private String bookName;

    public Book(String bookName) {
        this.bookName = bookName;
    }

    public Book() {

    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public static void printBooks(Printable[] printables) {
        for (Printable print:printables) {
            if (print instanceof Book)
                System.out.println(((Book) print).getBookName());
        }
    }

    @Override
    public void print() {
        System.out.println("It's a book: " + bookName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(getBookName(), book.getBookName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBookName());
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookName='" + bookName + '\'' +
                '}';
    }
}
