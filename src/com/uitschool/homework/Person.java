package com.uitschool.homework;

public class Person {

    String fullName;
    int age;

    Person(String fullName, int age) {

        if (age <= 0) {

            System.out.println("The negative or zero number is invalid, please input a " +
                    "natural number");
        } else {
            this.fullName = fullName;
            this.age = age;
        }

    }

    Person() {

    }

    private void move() {

        System.out.println("Person " + fullName + " moves");
    }

    private void talk() {

        System.out.println("Person " + fullName + " talks");
    }

    public static void main(String[] args) {

        Person alice = new Person();
        Person bob = new Person("Bob Peterson", 35);

        alice.move();
        alice.talk();

        bob.move();
        bob.talk();

    }
}
