package com.uitschool.homework;

import java.util.Scanner;

public class ProperAverage {

    //Вычислить среднее значение трех вещественных чисел
    // передаваемых на вход программы в качестве аргументов.

    public static void main(String[] args) {

        double sum = 0;
        float average = 0;
        Scanner sc;

        if (args.length == 0 || args == null) {

            System.out.println("Please input three real numbers into args array");

        } else if (args.length == 3) {

            for (String test : args) {

                sc = new Scanner(test);

                if (sc.hasNextDouble()) {

                    sum += sc.nextDouble();


                } else {

                    System.out.println("Please input real number in XX,XX format, or the average will be incorrect");
                    break;
                }

            }

        } else {

            System.out.println("There must be exactly 3 real numbers in args array");
        }

        average = (float) sum / args.length;

        System.out.println("Average value of 3 real numbers: " + average);

    }
}
