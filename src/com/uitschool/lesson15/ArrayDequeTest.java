package com.uitschool.lesson15;

import com.uitschool.lesson7.Phone;

import java.util.ArrayDeque;
import java.util.Deque;

public class ArrayDequeTest {

    public static void main(String[] args) {

        Deque<Phone> phoneDeque = new ArrayDeque<>();

        phoneDeque.offer(new Phone("+38974521","Samsung Galaxy S8", 745.58));
        phoneDeque.offer(new Phone("+3894974521","Apple iPhone XI", 578.45));
        phoneDeque.offer(new Phone("+698742521","Xiaomi Mi 10", 978.564));

        while(!phoneDeque.isEmpty()) {
            System.out.println(phoneDeque.poll());
        }
    }

}
