package com.uitschool.lesson12;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;

public class PropertiesTest {

    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println("Standard method");
        printInfo("", "");
        printInfo("en", "US");
        printInfo("uk", "UA");
        System.out.println("Alt method");
        printInfoAlt("", "");
        printInfoAlt("en", "US");
        printInfoAlt("uk", "UA");


    }

    private static void printInfo(String language, String country)
            throws UnsupportedEncodingException {
        Locale current = new Locale(language, country);
        ResourceBundle rb = ResourceBundle.getBundle("topic", current);
        String s1 = rb.getString("topic1");
        //   s1 = new String(s1.getBytes("ISO-8859-1"), "UTF-8");

        System.out.println(s1);

        String s2 = rb.getString("topic2");
        //   s2 = new String(s2.getBytes("ISO-8859-1"), "UTF-8");
        System.out.println(s2);

        String s3 = rb.getString("topic3");
        //   s2 = new String(s2.getBytes("ISO-8859-1"), "UTF-8");
        System.out.println(s3);

        System.out.println();
    }

    private static void printInfoAlt(String language, String country)
            throws UnsupportedEncodingException {
        Locale current = new Locale(language, country);
        ResourceBundle rb = ResourceBundle.getBundle("topic", current);
        for (String key : rb.keySet()) {
            String value = rb.getString(key);
            //value = new String(value.getBytes("ISO-8859-1"), "UTF-8");
            System.out.println(value);
        }
        System.out.println();
    }


}
