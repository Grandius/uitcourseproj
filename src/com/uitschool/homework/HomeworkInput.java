package com.uitschool.homework;

import java.util.Scanner;

public class HomeworkInput {

    //Создать программу, которая будет вычислять и выводить на экран
    // сумму двух целых чисел, введённых пользователем.
    // Если пользователь некорректно введёт хотя бы одно из чисел,
    // то сообщать об ошибке.
    public static void main(String[] args) {

        int firstNum = 0;
        int secondNum = 0;
        int intSum;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter first integer number");
        firstNum = returnInteger(input);
        //input = new Scanner(System.in);
        //input.nextLine();
        System.out.println("Enter second integer number");
        secondNum = returnInteger(input);
        intSum = firstNum + secondNum;
        System.out.println("Sum of first and second integer number: " + intSum);

    }

    static int returnInteger(Scanner sc) {

        int retInt;
        if (sc.hasNextInt()) {
            //retInt = Integer.getInteger(sc.nextLine());
            retInt = sc.nextInt();
            sc.nextLine();
            return retInt;
        } else if (!sc.hasNextInt()) {
            System.out.println("Error has occured, please input correct integer number");
            sc.nextLine();
            //sc = new Scanner(System.in);
            retInt = returnInteger(sc);
            return retInt;
        }
        return retInt = sc.nextInt();
    }
}