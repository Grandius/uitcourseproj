package com.uitschool.lesson2;

public class Literals {
    public static void main(String[] args) {

        /*double d1 = 3.45;
        float f1 = 34.45f;
        char c2 = 45;
        char с3 = '\n';
        boolean b = false;
        String s1 = "Hello!!";
        System.out.println("Hello World");
        System.out.println(b);
        System.out.println(d1);
        System.out.println(s1);*/
        System.out.println("Логический литерал: " + false);
        System.out.println("Целочисленный (16) литерал: " + 0XAb12);
        System.out.println("Целочисленный (10) литерал: " + 98);
        System.out.println("Целочисленный (8) литерал: " + 011);
        System.out.println("Целочисленный (2) литерал: " + 0b0111);
        System.out.println("Дробный литерал: " + 4.26);
        System.out.println("Символьный литерал: " + 'r');


    }
}
