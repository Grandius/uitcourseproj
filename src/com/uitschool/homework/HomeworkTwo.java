package com.uitschool.homework;


/*
 * Гирлянда
 */
public class HomeworkTwo {

    /*public static void main(String[] args) {
        int i1 = 23;
        System.out.println(Integer.toBinaryString(i1));
        String s1 = "Hello world!";
        String s2 = new String("Hello world!");
        String s3 = "Hello world!";
        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));
        System.out.println(s1 == s3);
        System.out.println(s1.equals(s3));
    }*/

    public static final int LAMPS_COUNT = 32;

    public static void main(String[] args) {
        int girlyanda = 45;
        runString(girlyanda, 20);
    }

    private static void runString(int girlyanda, int count) {
        for (int i = 0; i < count; i++) {
            if (girlyanda < 0) {
                girlyanda <<= 1;
                girlyanda++;
            } else {
                girlyanda <<= 1;
            }

            print(girlyanda);
        }
    }

    private static void print(int girlyanda) {
        String str = Integer.toBinaryString(girlyanda);
        if (str.length() < LAMPS_COUNT) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < LAMPS_COUNT - str.length(); i++) {
                stringBuilder.append("0");
            }
            str = stringBuilder.toString() + str;
        }
        System.out.println(str);
    }
}
