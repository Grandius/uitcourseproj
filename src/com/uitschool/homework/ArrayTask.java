package com.uitschool.homework;

/*Создать двухмерный массив 5х8 и инициализировать его с помощью блока для инициализации.
 *Найти максимальное и минимальное значение в каждой "строке" и записать эти значения в двухмерный массив 5х2.
 *Распечатать оба массива.*/

import java.util.Arrays;

public class ArrayTask {

    public static void main(String[] args) {

        int[][] integArray = {
                {1, 2, 3, 4, 5, 6, 7, 8},
                {9, 10, 11, 12, 13, 14, 15, 16},
                {17, 18, 19, 20, 21, 22, 23, 24},
                {25, 26, 27, 28, 29, 30, 31, 32},
                {33, 34, 35, 36, 37, 38, 39, 40}};
        int[][] minMaxArrayOne = {
                {0, 0},
                {0, 0},
                {0, 0},
                {0, 0},
                {0, 0}};
        int[][] minMaxArrayTwo = {
                {0, 0},
                {0, 0},
                {0, 0},
                {0, 0},
                {0, 0}};
        /*int[] testArray = integArray[0];
        System.out.println(Arrays.toString(testArray));*//*
         *//* int[] testArrayTwo = {4,5,2,8,9,12,4537};
        Arrays.sort(testArrayTwo);
        System.out.println(Arrays.toString(testArrayTwo));*//*
        //int[][] minMaxArray = new int[5][2];
        int count = minMaxArray[0].length;
        System.out.println("Length of array line: " + count);
        count = integArray.length;
        System.out.println("Length of array line: " + count);*/
        System.out.println("Initial array");
        for (int[] outputArray : integArray) {
            System.out.println(Arrays.toString(outputArray));
        }
        //обычный for
        for (int i = 0; i < integArray.length; i++) {
            int[] subArray = integArray[i];
            Arrays.sort(subArray);
            minMaxArrayOne[i][0] = subArray[0];
            minMaxArrayOne[i][minMaxArrayOne[0].length - 1] = subArray[subArray.length - 1];
        }
        //с помощью for each
        int i = 0;
        for (int[] subArray : integArray) {
            Arrays.sort(subArray);
            minMaxArrayTwo[i][0] = subArray[0];
            minMaxArrayTwo[i][minMaxArrayTwo[0].length - 1] = subArray[subArray.length - 1];
            i++;
        }
        //блок кода, показывающий, что оба метода равноценны
        System.out.println("First minMax array");
        for (int[] outputArray : minMaxArrayOne) {
            System.out.println(Arrays.toString(outputArray));
        }

        System.out.println("Second minMax array");
        for (int[] outputArray : minMaxArrayTwo) {
            System.out.println(Arrays.toString(outputArray));
        }
    }
}
