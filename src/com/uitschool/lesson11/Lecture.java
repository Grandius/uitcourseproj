package com.uitschool.lesson11;

public class Lecture {
    public static void main(String[] args) {

        Double d1 = 6.23;
        Double d2 = new Double("45.7412");
        Double d3 = Double.parseDouble("645");
        Double d4 = new Double("45.7413f");
        Double d5 = 567.356;
        Double d6 = Double.valueOf("2345");

        /*System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);
        System.out.println(d4);
        System.out.println(d5);
        System.out.println(d6);

        Boolean b1 = Boolean.getBoolean("true");

        switch (b1) {
            case (d1<d2):
                System.out.println("True");
        }*/

        check("I like Java");
        check("I hate Python!!!");
        check("I love C#");
        check("I like Java!!!");



    }

    public static void check (String s) {
        System.out.println(s);
        System.out.print("It ends with !!! " + s.endsWith("!!!"));
        System.out.print(", it contains I Like " + s.startsWith("I like"));
        System.out.print(", it contains Java " + s.contains("Java"));
        System.out.println();
    }
}
