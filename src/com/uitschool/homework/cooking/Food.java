package com.uitschool.homework.cooking;

public class Food {

    private String foodName;

    public Food(String foodName) {
        this.foodName = foodName;
    }

    public Food() {

    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Food food = (Food) o;

        return getFoodName() != null ? getFoodName().equals(food.getFoodName()) : food.getFoodName() == null;
    }

    @Override
    public int hashCode() {
        return getFoodName() != null ? getFoodName().hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Food{" +
                "foodName='" + foodName + '\'' +
                '}';
    }

    public void prepare(Cookable c, String str) {

        c.cook(str);
        System.out.println("Name of the whole dish: "+getFoodName());
    }
}
