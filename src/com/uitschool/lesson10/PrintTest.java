package com.uitschool.lesson10;

public class PrintTest {

    public static void main(String[] args) {

        Book b1 = new Book("Some stories");
        Book b2 = new Book("Some stories 2: Electric Boogaloo");
        Magazine m1 = new Magazine("Weekly News");
        Magazine m2 = new Magazine("Daily News");
        Printable[] printables = {b1, b2, m1, m2};

        /*for (Printable test : printables) {
            test.print();
        }*/
        Magazine.printMagazines(printables);
        Book.printBooks(printables);

    }
}
